<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Home page */
Route::get('/', function () {
    return view('pages/index');
})->name('index.show');

/* Product page */
Route::get('/product', function () {
    return view('product/product');
});

Route::get('/category', function () {
    return view('category/list');
});
Route::get('/popup', function () {
    return view('elements/popups');
});
Route::get('/colorpopup', function () {
    return view('elements/colorpopup');
});
Route::get('/kontakt', function () {
    return view('pages/contact');
});

Route::get('/nakup', function () {
    return view('nakupny proces/shopping');
})->name('shopping.buy.show');
Route::get('/doprava', function () {
    return view('nakupny proces/doprava');
})->name('shopping.shipping.show');
Route::get('/suhrn', function () {
    return view('nakupny proces/suhrn');
})->name('shopping.summary.show');
Route::get('/form', function () {
    return view('nakupny proces/shippingForm');
})->name('shopping.form.show');

Route::get('/profile', function () {
    return view('profile/myAccount');
})->name('profile.myaccount.show');

Route::get('/profile-orders', function () {
    return view('profile/orders');
})->name('profile.myorders.show');

Route::get('/profile-info', function () {
    return view('profile/personalInfo');
})->name('profile.personalinfo.show');

Route::get('/profile-facture', function () {
    return view('profile/facture');
})->name('profile.showfactures.show');

Route::get('/profile-password', function () {
    return view('profile/passwordChange');
})->name('profile.passwordchange.show');

Route::get('/profile-uprava-fakturacnych-udajov', function () {
    return view('profile/factureForm');
})->name('profile.editfactureinfo');

Route::get('/profile-uprava-dodacich-udajov', function () {
    return view('profile/editadress');
})->name('profile.editadressinfo');