$('.topbar__trigger').click(function() {
    $('.aside__topmenu').addClass('active');
    $('.overlay__white').addClass('active');
});

$('.overlay__white, .aside__topmenu__header__close').click(function() {
    $('.aside__topmenu').removeClass('active');
    $('.overlay__white').removeClass('active');
});

$('.navbar__trigger').click(function() {
    $('.main').toggleClass('nav-active');
    $('.navbar__trigger').toggleClass('active');
})

$('#accordion-2 li').click(function() {
    $(this).has('.fa').toggleClass('active');
});

$('#filter .filterBlock').click(function() {

	$('#filter .filterBlock-container').removeClass('active');
	$(this).find('.filterBlock-container').toggleClass('active');
});


function hideMenu() {

    var screenWidth = $( window ).width();

    if (screenWidth <= 1240) {
        $('.main').removeClass('nav-active');
    }
}
    hideMenu();

    $( window ).resize(function(){
        hideMenu();
    });




