<header class="border-head">
    <div class="background" id="background" onclick="closeNav()">

    </div>
    <div class="contact-bar d-none d-xl-block">
        <div class="container_left">
            <ul class="menu_links float-left">
                <li class="nav-item"> <a href="#">Predajne</a> </li>
                <li>|</li>
                <li> <a href="#">Klub lulubee</a></li>
                <li>|</li>
                <li> <a href="#">blog</a></li>
                <li>|</li>
                <li> <a href="#">košík</a></li>
                <li>|</li>
                <li> <a href="#">pomoc pri nákupe</a></li>
                <li>|</li>
                <li> <a href="#">10€ na prvý nákup</a></li>
            </ul>
        </div>

        <div class="container_right">
            <ul class="menu_personal float-right">
                <li> <i class="fa fa-phone"> </i> <a href="tel:+421 911 012 064" > +421 911 012 064 </a> </li>
                <li> <i class="fa fa-clock-o"></i> PO-PIA: 10:00-18:00 </li>
                <li>SO: 10:00-13:00</li>
            </ul>
        </div>
    </div>



    <nav class="navbar navbar-expand-lg navbar-light">

        {{--logo na malom displeji--}}
        <div class="logo d-lg-none float-right" style="margin-top: 20px;">
            <a class="" href="#"><span style=""><img src="images/logo.png" alt="obrazok" width="160"></span></a>
        </div>


        <div class="sidenav" id="mySidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <a href="#">Kočíky</a>
            <a href="#">Autosedačky</a>
            <a href="#">Detská izba</a>
            <a href="#">Hračky</a>

            {{--<h2 class="profilInfo" style="color:white;margin-left: 30px;margin-top:30px;">Profil</h2>--}}
            {{--<a href="#" class="favorite">Oblubene</a>--}}
            {{--<a href="#" class="userInfo">Profil</a>--}}
            {{--<a href="#" class="cart">Kosik</a>--}}
            {{--<a href="#" class="hladat">Hladat</a>--}}
        </div>

        <ul class="d-lg-none d-md-block menu ml-auto float-right">
            <li> <a href="#"> <i class="far fa-heart "> </i> </a> </li>
            <li> <a href="#"> <i class="far fa-user"> </i>  </a> </li>
            <li> <a href="#"> <i class="fa fa-shopping-bag "> </i>   </a> </li>
            <li> <a href="#"> <i class="fa fa-search"> </i> </a> </li>
            <li><span onclick="openNav()" class="navbar-toggler-icon d-block d-lg-none"></span></li>
        </ul>


        <div class="collapse navbar-collapse mr-auto " id="navbarNavAltMarkup">

            <ul class="navbar-nav mr-auto ">
                <li class="nav-item text-sm-center"> <a href="#">Kočíky</a> </li>
                <li class="nav-item text-sm-center"> <a href="#">Autosedačky</a></li>
                <li class="nav-item text-sm-center"> <a href="#">Detská izba</a></li>
                <li class="nav-item text-sm-center"> <a href="#">Hračky</a></li>
            </ul>

            <div class="logo d-none d-xl-block">
                <a href="#"><span style=""><img src="images/logo.png" alt="obrazok" width="160"></span></a>
            </div>

            <form class="form-inline ml-auto mt-lg-2">
                <ul class="d-none d-sm-block">
                    <li> <a href="#"> <i class="far fa-heart "> </i> </a> </li>
                    <li> <a href="#"> <i class="far fa-user"> </i>  </a> </li>
                    <li> <a href="#"> <i class="fa fa-shopping-bag "> </i>   </a> </li>
                    <li><a href="#"> <i class="fa fa-search"> </i></a></li>
                    <input class="search" type="text" placeholder="Vyhľadávanie...">
                </ul>
            </form>
        </div>
    </nav>

</header>
<script>

    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("background").style.display = "block";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("background").style.display = "none";
    }

</script>