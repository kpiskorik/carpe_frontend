<footer class="footer">
    <div class="container-fluid">
        <div class="row justify-content-center first-footer">
            <div class="col-lg-4 text-center">
                <div  class="mt-4" style="width: 200px; margin:0 auto;">
                    <img src="images/Vector%20Smart%20Object.png" alt="vectorLogo" width="40px;" style="float:left;">
                    <h1 style="text-transform: uppercase; font-size:22px;padding-top: 10px;margin-left: 60px;">Newsletter</h1>
                </div>
            </div>
            <div class="col-lg-4 text-center inputEmail mt-2">
                <input type="text" placeholder="Napíšte svoj email" style="height:35px;border: none">
                <a href="" class="btn btn-secondary border-0" style="height: 35px; margin-bottom: 4px;">Prihlásiť sa na odber</a>
            </div>
            <div class="col-lg-4 text-center mt-sm-4">
                <div style="width: 300px;margin:0px auto;">
                    <img src="images/money.png" alt="money" style="float:left;">
                    <p style="width: 150px;text-transform: uppercase;text-align: left; margin-left: 165px;margin-bottom: 5px;">Pozvite priateľov a získajte</p>
                    <span style="font-size: 40px;margin-top: 0px;margin-bottom: 0;line-height:0.8">10€</span>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="container-fluid images">
    <img src="images/logo1.jpg" alt="brands" style="margin-left:5%;padding-top:50px;padding-bottom: 50px;width: 150px;">
    <img src="images/logo2.jpg" alt="brands" style="margin-left:5%;padding-top:50px;padding-bottom: 50px;width: 150px;">
    <img src="images/logo3.jpg" alt="brands" style="margin-left:5%;padding-top:50px;padding-bottom: 50px;width: 150px;">
    <img src="images/logo1.jpg" alt="brands" style="margin-left:5%;padding-top:50px;padding-bottom: 50px;width: 150px;">
    <img src="images/logo2.jpg" alt="brands" style="margin-left:5%;padding-top:50px;padding-bottom: 50px;width: 150px;">
    <img src="images/logo3.jpg" alt="brands" style="margin-left:5%;padding-top:50px;padding-bottom: 50px;width: 150px;">
</div>



<footer class="footer">
    <div class="row justify-content-center text-center">
        <div class="odstavec col-8 col-lg-2  col-sm-6 ml-sm-auto">
            <h1>O lulubee</h1>
            <ul class="downList">
                <li><a href="#">O nás</a></li>
                <li><a href="#">Kontakt</a></li>
                <li><a href="#">Značky</a></li>
                <li><a href="#">Služby pre zákazníkov</a></li>
                <li><a href="#">Servis kočíkov</a></li>
                <li><a href="#">Hľadáme do lulubee tímu</a></li>
            </ul>
        </div>

        <div class="odstavec col-8 col-lg-2 col-sm-6 ml-sm-auto">
            <div class="odstavec">
                <h1>Informácie</h1>
                <ul class="downList">
                    <li><a href="#">Obchodné podmienky</a></li>
                    <li><a href="#">Ochrana osobných údajov</a></li>
                    <li><a href="#">Reklamácie</a></li>
                    <li><a href="#">Spôsob dopravy a platby</a></li>
                </ul>
            </div>
        </div>

        <div class="odstavec col-10 col-lg-2 ml-sm-auto mr-sm-auto text-center">
            <img src="images/Vector%20Smart%20Object.png" alt="logo" width="50px">
            <p>Pridajte sa k našim sociálnym sieťam</p>
            <div class="social_media_links center-block">
                <ul class="menu">
                    <li><a href="" class="fab fa-facebook-square social_link" style="color:white;"></a></li>
                    <li><a href="" class="fab fa-instagram social_link" style="color:white;"></a></li>
                    <li><a href="" class="fab fa-google-plus-g social_link" style="color:white;"></a></li>
                </ul>
            </div>
        </div>

        <div class="odstavec col-8 col-lg-2 col-sm-6 ml-sm-auto">
            <h1>Otváracie hodiny</h1>
            <ul class="downList">
                <li>Po / 14:00-19:00</li>
                <li>Ut-Pi / 11:00-19:00</li>
                <li>So / 10:00-17:00</li>
                <li>Ne / Zatvorené</li>
            </ul>
        </div>

        <div class="odstavec col-8 col-lg-2 col-sm-6 ml-sm-auto">
            <h1>Adresa predajne</h1>
            <ul class="downList">
                <li>Lulubee</li>
                <li>Dvorakovo nabrezie 8</li>
                <li>811 02 Bratislava</li>
                <li>Email - info@lulubee.sk</li>
                <li>Tel. +421 911 012 064</li>
            </ul>
        </div>

    </div>
</footer>
