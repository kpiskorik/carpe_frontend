@extends('home')
@section('title', 'Home')

@section('content')
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Domov</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Nákup</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 mb-5">
                <div class="container-fluid" style="margin-left: 10%;">
                    <ul class="menu">
                        <a href='{{route('shopping.buy.show')}}'>
                            <li class="mr-5 ">
                                <span class="header-shopping-num">1.</span>
                                <span class="header-shopping-text">Košík</span>
                            </li>
                        </a>
                        <a href='{{route('shopping.shipping.show')}}'>
                            <li class="mr-5">
                                <span class="header-shopping-num">2.</span>
                                <span class="header-shopping-text">Doprava a platba</span>
                            </li>
                        </a>
                        <a href='{{route('shopping.form.show')}}'>
                            <li class="mr-5">
                                <span class="header-shopping-num">3.</span>
                                <span class="header-shopping-text">Dodacie údaje</span>
                            </li>
                        </a>
                        <a href='{{route('shopping.summary.show')}}'>
                            <li class="active-link  mr-5">
                                <span class="header-shopping-num">4.</span>
                                <span class="header-shopping-text">Potvrdenie objednávky</span>
                            </li>
                        </a>
                    </ul>
                </div>
            </div>

            <div class="cart-wrapper col-12">
                <div class="row">
                    <div class="col-12">
                        <h1 class="text-center">Súhrn objednávky</h1>
                    </div>
                    <div class="col-12 col-md-6 mt-5">
                        <h3 style="margin-bottom: 30px;">Fakturačná adresa</h3>
                        <div class="ml-3">
                            <span><strong>Meno a priezvisko: </strong> Tomas Pudis</span>
                            <br>
                            <span><strong>Ulica a číslo domu: </strong>gdgdfsds </span>
                            <br>
                            <span><strong>PSČ a mesto: </strong> 77777, gdfhfdgh -</span>
                            <br>
                            <span><strong>Telefón: </strong> 908336829</span>
                            <br>
                            <span><strong>E-mail: </strong>tomas.pudis@madviso.com</span>
                            <br>
                            <span><strong>Krajina: </strong>Slovakia</span>

                        </div>

                        <h3 style="margin-top:50px;">Dodacia adresa</h3>
                        <p class="mt-3 ml-3">Rovnaká ako fakturačná</p>

                        <h3 style="margin-top:50px;">Doprava a platba</h3>
                        <div class="ml-3">
                            <span><strong>Spôsob dopravy: </strong>Osobný odber</span>
                            <br>
                            <span><strong>Spôsob platby: </strong>Osobný odber</span>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 mt-5">
                        <h3 class="mb-3">Vaša objednávka</h3>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Produkt</th>
                                    <th>Cena spolu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="product-name">Sportful Fiandre WS návleky na tretry čierne<strong class="product-quantity"> × 1</strong></td>
                                    <td class="product-total">69.90€</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr class="cart-subtotal">
                                    <th>Spolu bez DPH:</th>
                                    <td>58.25 €</td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>DPH:</th>
                                    <td>11.65 €</td>
                                </tr>
                                <tr class="shipping">
                                    <th>Doprava</th>
                                    <td class="shipping-holder">0.00 €</td>
                                </tr>
                                <tr class="order-total">
                                    <th>Spolu s DPH</th>
                                    <td data-title="Total"><strong><span class="amount total-holder">69.90 €</span></strong></td>
                                </tr>
                            </tfoot>
                        </table>

                        <p style="margin-top:100px;">Stlačením „Objednať s povinnosťou platby“ súhlasíte s obchodnými podmienkami a spracovaním osobných údajov</p>
                    </div>
                    <div class="col-12" style="margin-top:20px;">
                        <a class="btn btn-primary btn float-left pading-normal" href="#">Dodacie údaje</a>
                        <a class="btn btn-primary btn float-left float-md-right" href="#" style="width: 220px;">Objednať s povinnosťou platby</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
