@extends('home')
@section('title', 'Home')

@section('content')
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Domov</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Nákup</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 mb-5">
                <div class="container-fluid" style="margin-left: 10%;">
                    <ul class="menu">
                        <a href='{{route('shopping.buy.show')}}'>
                            <li class="active-link mr-5 ">
                                <span class="header-shopping-num">1.</span>
                                <span class="header-shopping-text">Košík</span>
                            </li>
                        </a>
                        <a href='{{route('shopping.shipping.show')}}'>
                            <li class="mr-5">
                                <span class="header-shopping-num">2.</span>
                                <span class="header-shopping-text">Doprava a platba</span>
                            </li>
                        </a>
                        <a href='{{route('shopping.form.show')}}'>
                            <li class="mr-5">
                                <span class="header-shopping-num">3.</span>
                                <span class="header-shopping-text">Dodacie údaje</span>
                            </li>
                        </a>
                        <a href='{{route('shopping.summary.show')}}'>
                            <li class="mr-5">
                                <span class="header-shopping-num">4.</span>
                                <span class="header-shopping-text">Potvrdenie objednávky</span>
                            </li>
                        </a>

                    </ul>
                </div>

            </div>

            <div class="cart-wrapper col-12">
                <table class="shop_table table-responsive-md">
                    <thead>
                        <tr>
                            <th></th>
                            <th colspan="2">Produkt</th>
                            <th>Cena</th>
                            <th>Množstvo</th>
                            <th>Spolu</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="del-cell">
                                <a href="#"><span style="margin:40px 40px;"><i class="fas fa-times"></i></span></a>
                            </td>
                            <td class="img-cell">
                                <a href="#" class="shopping-link">
                                    <img src="images/Trip%20Trapp%202.png" alt="product" width="80px;">
                                </a>
                            </td>
                            <td class="description-cell">
                                <a href="#"><span class="description-text">Kocik bugaboo</span></a>
                                <span class="description-code">(Kód produktu: 64985) </span>
                                <span class="description-text"> Vybraná varianta</span>
                                <span class="description-value"> Farba: zelená</span>
                            </td>
                            <td class="">
                                18,30 €
                            </td>
                            <td>
                                <div class="count">
                                    <ul class="menu" style="padding-left: 0px;">
                                        <li><a href="#"><span class="fa fa-minus-circle"></span></a></li>
                                        <li>1</li>
                                        <li><a href="#"><span class="fa fa-plus-circle"></span></a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                164,70 €
                            </td>
                        </tr>
                        <tr>
                            <td class="del-cell">
                                <a href="#"><span style="margin:40px 40px;"><i class="fas fa-times"></i></span></a>
                            </td>
                            <td class="img-cell">
                                <a href="#" class="shopping-link">
                                    <img src="images/Trip%20Trapp%202.png" alt="product" width="80px;">
                                </a>
                            </td>
                            <td class="description-cell">
                                <a href="#"><span class="description-text">Kocik bugaboo</span></a>
                                <span class="description-code">(Kód produktu: 64985) </span>
                                <span class="description-text"> Vybraná varianta</span>
                                <span class="description-value"> Farba: zelená</span>
                            </td>
                            <td class="">
                                18,30 €
                            </td>
                            <td>
                                <div class="count">
                                    <ul class="menu" style="padding-left: 0px;">
                                        <li><a href="#"><span class="fa fa-minus-circle"></span></a></li>
                                        <li>1</li>
                                        <li><a href="#"><span class="fa fa-plus-circle"></span></a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                164,70 €
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div class="col-12">
                <h2 class="float-right mt-3 mr-1"> Tovar spolu: 164,70 €</h2>
            </div>
            <div class="col-12 mt-5">
                <p style="float: right;width: 360px;"><i>Ak máte zľavový kód a radi by ste ho použili zadajte ho sem. Po potvrdení Vám bude pridelená príslušná zľava.</i></p>
            </div>
            <div class="col-12">
                <a class="btn btn-primary btn float-right" href="#" style="margin-left: 0px;padding: 5px !important;margin-right: 0px;">Použi kód</a>
                <input type="text" class="float-right" style="margin-top: 5px;height: 35px;">
            </div>
            <div class="col-12 mt-5">
                <a class="btn btn-primary btn float-left pading-normal" href="#">Späť k nákupu</a>
                <a class="btn btn-primary btn float-left float-md-right" href="#" style="width: 160px;margin:0px;padding: 5px 2px;">Dokončiť objednávku </a>
            </div>
        </div>

    </div>


@endsection
