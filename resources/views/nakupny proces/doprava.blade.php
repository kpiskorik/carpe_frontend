@extends('home')
@section('title', 'Home')

@section('content')
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Domov</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Nákup</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 mb-5">
                <div class="container-fluid" style="margin-left: 10%;">
                    <ul class="menu">
                        <a href='{{route('shopping.buy.show')}}'>
                            <li class=" mr-5 ">
                                <span class="header-shopping-num">1.</span>
                                <span class="header-shopping-text">Košík</span>
                            </li>
                        </a>
                        <a href='{{route('shopping.shipping.show')}}'>
                            <li class="active-link mr-5">
                                <span class="header-shopping-num">2.</span>
                                <span class="header-shopping-text">Doprava a platba</span>
                            </li>
                        </a>
                        <a href='{{route('shopping.form.show')}}'>
                            <li class="mr-5">
                                <span class="header-shopping-num">3.</span>
                                <span class="header-shopping-text">Dodacie údaje</span>
                            </li>
                        </a>
                        <a href='{{route('shopping.summary.show')}}'>
                            <li class="mr-5">
                                <span class="header-shopping-num">4.</span>
                                <span class="header-shopping-text">Potvrdenie objednávky</span>
                            </li>
                        </a>
                    </ul>
                </div>

            </div>

            <div class="cart-wrapper col-12">

                <div class="row doprava-mid">

                    {{--<span class="or-text">a</span>--}}

                    <div class="col-12 col-md-6 doprava border-right" style="padding-right:80px;">
                        <h2>Doprava</h2>
                        <div class="ship-box">
                            <h4>Kuriér</h4>
                            <p> <strong>10.00€</strong> </p>
                        </div>
                        <div class="ship-box active">
                            <h4>Osobný odber</h4>
                            <p> <strong>0.00€</strong> </p>
                        </div>
                    </div>


                    <div class="col-12 col-md-6 doprava" style="padding-left:80px;">
                        <h2>Platba</h2>
                        <div class="ship-box">
                            <h4>Osobný odber</h4>
                            <p>Osobný odber v našej prevádzke na Leškova 8 v Bratislave.</p>
                        </div>
                        <div class="ship-box ">
                            <h4>Prevodom</h4>
                            <p>Platba prevodom na náš účet.</p>
                        </div>
                        <div class="ship-box active">
                            <h4>Dobierka</h4>
                            <p>Doručíme vám tovar kuriérom na dobierku.</p>
                        </div>
                    </div>

                    <div class="col-12" style="margin-top:100px;margin-bottom: 50px;">
                        <a class="btn btn-primary btn float-left pading-normal" href="#">Fakturačné údaje</a>
                        <a class="btn btn-primary btn float-left float-md-right" href="#" style="width: 160px;margin:0px;padding: 5px 2px;">Dokončiť objednávku </a>
                    </div>

                    <div class="col-12">
                        <p><strong>*Zobrazujú sa možnosti určené pre krajinu: Slovakia.</strong></p>
                    </div>

                </div>

            </div>


        </div>

    </div>


@endsection
