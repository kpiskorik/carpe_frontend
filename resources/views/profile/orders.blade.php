@extends('home')
@section('title', 'Home')

@section('content')

    <div class="row">
        <div class="col-12 ml-3 mt-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Domov</a></li>
                    <li class="breadcrumb-item"><a href="#">Môj účet</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Moje objednávky</li>
                </ol>
            </nav>
        </div>

        <div class="col-12 mb-5">
            <h1 class="text-center"  style="font-weight: 700;">Moje objednávky</h1>
        </div>

        <div class="col-lg-4 col-xl-3 col-md-5 col-12 pl-5 pr-5">
            <ul class="nav flex-column profile-navbar">
                <li class="nav-item">
                    <a class="nav-link active" href='{{route('profile.myaccount.show')}}'>Nástenka <i class="fas fa-palette "></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href='{{route('profile.myorders.show')}}'>Moje objednávky <i class="fas fa-shopping-basket"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('profile.showfactures.show')}}">Moje faktúry <i class="far fa-file-archive"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('profile.personalinfo.show')}}">Osobné údaje <i class="fas fa-home"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('profile.passwordchange.show')}}">Zmena hesla <i class="fas fa-key"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">Odhlásenie <i class="fas fa-sign-out-alt"></i></a>
                </li>
            </ul>
        </div>
        <div class="col-lg-8 col-md-7 col-12 pl-5 mt-5 mt-md-0">
            <table class="table table-borderless table-responsive-sm">
                <thead class="table-borderless">
                    <tr>
                        <th>ČÍSLO</th>
                        <th>VYTVORENÁ</th>
                        <th>STAV</th>
                        <th>CELKOM K ÚHRADE</th>
                        <th>AKCIE</th>
                    </tr>
                </thead>
            </table>
            <p style="text-align: center; font-style:10px;">Nemáte žiadne objednávky</p>
        </div>
    </div>

@endsection
