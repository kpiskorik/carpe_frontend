@extends('home')
@section('title', 'Home')

@section('content')

    <div class="row">
        <div class="col-12 ml-3 mt-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Domov</a></li>
                    <li class="breadcrumb-item"><a href="#">Môj účet</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Zmena hesla</li>
                </ol>
            </nav>
        </div>

        <div class="col-12 mb-5">
            <h1 class="text-center" style="font-weight: 700;">Zmena hesla</h1>
        </div>

        <div class="col-lg-4 col-xl-3 col-md-5 col-12 pl-5 pr-5">
            <ul class="nav flex-column profile-navbar">
                <li class="nav-item">
                    <a class="nav-link active" href='{{route('profile.myaccount.show')}}'>Nástenka <i class="fas fa-palette "></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href='{{route('profile.myorders.show')}}'>Moje objednávky <i class="fas fa-shopping-basket"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('profile.showfactures.show')}}">Moje faktúry <i class="far fa-file-archive"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('profile.personalinfo.show')}}">Osobné údaje <i class="fas fa-home"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{route('profile.passwordchange.show')}}">Zmena hesla <i class="fas fa-key"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">Odhlásenie <i class="fas fa-sign-out-alt"></i></a>
                </li>
            </ul>
        </div>
        <div class="col-lg-8 col-md-7 col-12 pl-5 mt-5 mt-md-0">
            <form>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control form-control-lg" id="inputEmail" value="tomas.pudis@madviso.com">
                    </div>
                    <h3 style="font-weight:700;">Zmena hesla</h3>
                    <div class="form-group col-md-12">
                        <label for="oldPasw" class="">Doterajšie heslo:</label>
                        <input type="password" class="form-control form-control-lg" id="oldPasw">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="newPasw">Nové heslo:</label>
                        <input type="password" class="form-control form-control-lg" id="newPasw">
                    </div>
                    <div class="form-group col-12">
                        <label for="inputMessage">Potvrdte nové heslo:</label>
                        <input type="text" class="form-control form-control-lg" id="inputMessage">
                    </div>
                </div>

                <button type="submit" class="btn btn-primary float-left ml-0">Uložiť zmeny</button>

            </form>
        </div>
    </div>

@endsection
