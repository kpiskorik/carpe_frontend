@extends('home')
@section('title', 'Home')

@section('content')

    <div class="container-fluid banners mt-4">
        <div class="row text-center">
            <div class="col-md-6 container_images">
                <img src="{{asset('assets/images/home-new.jpg')}}" class="img-fluid" >
                <a href="#"><div class="img_label">Detská izba 40 % zľava</div></a>
            </div>
            <div class="col-md-6 container_images">
                <img src="{{asset('assets/images/home-new.jpg')}}" class="img-fluid">
                <a href="#"><div class="img_label right ">Detská izba 40 % zľava</div></a>
            </div>

        </div>
    </div>

    <div class="container-fluid main_content_category">
        <div class="row category_title">
            <div class="col-12">
                <h2 style="text-align: center;font-size: 28px">Obľubené kategórie</h2>
            </div>
        </div>
        <div class="row category_products">
            <div class="col-md-12">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#kociky" role="tab" aria-controls="home" aria-selected="false"><h2>Kočíky</h2></a>
                    </li>
                    <li style="color:#7f7f7f" class="mt-2"><h2>|</h2></li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#sedacky" role="tab" aria-controls="profile" aria-selected="false"><h2>Sedačky</h2></a>
                    </li>
                    <li style="color:#7f7f7f" class="mt-2"><h2>|</h2></li>


                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#hracky" role="tab" aria-controls="contact" aria-selected="false"><h2>Hračky</h2></a>
                    </li>
                    <li style="color:#7f7f7f" class="mt-2"><h2>|</h2></li>

                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#izba" role="tab" aria-controls="contact" aria-selected="false"><h2>Izba</h2></a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="tab-content container-fluid product__items pl-md-5 pr-md-5" id="myTabContent">
            <div class="tab-pane fade show active ml-auto mr-auto" id="kociky" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row pl-0">


                    <div class="product col-12 col-sm-6 col-lg-3 ">

                        <a href="#" class="arrow__left d-none d-lg-block"><i class="fas fa-angle-left fa-3x"></i></a>

                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                        <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons row">
                            <a class="btn btn-primary btn-sm float-right col-6" href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-2 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right col-6" href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-2 mr-3">Kupit</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3 ">
                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3">
                        <div class="top label">Top produkt</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3">

                        <a href="#" class="arrow__right d-none d-lg-block"><i class="fas fa-angle-right fa-3x"></i></a>

                        <div class="novinka label">Novinka</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade" id="sedacky" role="tabpanel" aria-labelledby="contact-tab">
                <div class="row pl-0">

                    <div class="product col-12 col-sm-6 col-lg-3 ">

                        <a href="#" class="arrow__left d-none d-lg-block"><i class="fas fa-angle-left fa-3x"></i></a>

                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                        <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3 ">
                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3">
                        <div class="top label">Top produkt</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3">

                        <a href="#" class="arrow__right d-none d-lg-block"><i class="fas fa-angle-right fa-3x"></i></a>

                        <div class="novinka label">Novinka</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade" id="hracky" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row pl-0">

                    <div class="product col-12 col-sm-6 col-lg-3 ">

                        <a href="#" class="arrow__left d-none d-lg-block"><i class="fas fa-angle-left fa-3x"></i></a>

                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                        <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3 ">
                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3">
                        <div class="top label">Top produkt</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3">

                        <a href="#" class="arrow__right d-none d-lg-block"><i class="fas fa-angle-right fa-3x"></i></a>

                        <div class="novinka label">Novinka</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                </div>

            </div>
            <div class="tab-pane fade" id="izba" role="tabpanel" aria-labelledby="contact-tab">
                <div class="row pl-0">

                    <div class="product col-12 col-sm-6 col-lg-3 ">

                        <a href="#" class="arrow__left d-none d-lg-block"><i class="fas fa-angle-left fa-3x"></i></a>

                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                        <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3 ">
                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3">
                        <div class="top label">Top produkt</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-sm-6 col-lg-3">

                        <a href="#" class="arrow__right d-none d-lg-block"><i class="fas fa-angle-right fa-3x"></i></a>

                        <div class="novinka label">Novinka</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li>(0)</li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                </div>
            </div>
        </div>

        <div class="row category_title">
            <div class="col-12">
                <h2 style="text-align: center;font-size: 28px;margin-top: 20px;">Obľubené produkty </h2>
            </div>
        </div>

        <div class="row category_products pl-md-5 pr-md-5">

            <div class="product col-12 col-sm-6 col-lg-3 ">

                <a href="#" class="arrow__left d-none d-lg-block"><i class="fas fa-angle-left fa-3x"></i></a>

                <div class="akcia label">Akcia</div>
                <div class="zlava label">20%</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li>(0)</li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3 ">
                <div class="akcia label">Akcia</div>
                <div class="zlava label">20%</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li>(0)</li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3">
                <div class="top label">Top produkt</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li>(0)</li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3">

                <a href="#" class="arrow__right d-none d-lg-block"><i class="fas fa-angle-right fa-3x"></i></a>

                <div class="novinka label">Novinka</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li>(0)</li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

        </div>
    </div>

    <div class="container-fluid banners">
        <h2 style="text-align: center;font-size: 28px;margin-top: 20px;">Vyberáme pre vás</h2>
        <div class="row mt-5">
            <div class="col-lg-6 container_images text-center img-fluid">
                <img src="{{asset('assets/images/baby-img.jpg')}}" class=" img-fluid mb-lg-5" style="max-height: 400px;">
                <a href="#"><div class="img_label small">Detská postielka</div></a>
            </div>

            <div class="product smallerProduct col-12 col-sm-6 col-lg-3 ml-0 ml-md-5 ml-xl-0">
                <div class="akcia label">Akcia</div>
                <div class="zlava label">20%</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li>(0)</li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product smallerProduct col-12 col-sm-6 col-lg-3 ">
                <div class="akcia label">Akcia</div>
                <div class="zlava label">20%</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li>(0)</li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

        </div>
    </div>
    <div class="clearfix"></div>

    <div class="container-fluid services">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center popis_servis">
                <img src="{{asset('assets/images/services/services-car.jpg')}}"  class="img-fluid services-img" alt="" width="75px">
                <h3>Doprava zdarma od 100€</h3>
                <p>Pri nákupe akéhokoľvek produktu z našej ponuky, získavate, ako bonus dopravu k vám domov úplne zdarma.</p>
            </div>

            <div class="col-md-4 text-center popis_servis">
                <img src="{{asset('assets/images/services/services-map.jpg')}}" class="img-fluid services-img" alt="" width="75px">
                <h3>Osobný odber</h3>
                <p>Našu kamennú predajňu nájdete na adrese <span class="adress">Dvořákovo nábrežie 8, 811 02 Bratislava</span></p>
            </div>

            <div class="col-md-4 text-center popis_servis">
                <img src="{{asset('assets/images/services/services-kosik.jpg')}}" class="img-fluid services-img" alt="" width="75px">
                <h3>Servis kočíkov</h3>
                <p>Okrem predaja prémiového tovaru ponúkame tiež služby týkajúce sa čistenia kočíkov, skladania zakúpeného tovaru, jeho vypožičania a iné.</p>
            </div>
        </div>
    </div>

    <div class="blog container-fluid mb-0">
        <div class="row blog_heading">
            <div class="col-md-12 text-center">
                <h2 style="text-align: center;font-size: 28px;margin-bottom: 20px;">Blog</h2>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-4 text-sm-center text-md-left clanok">
                <a href="#"><div class="card-img-top" style="background:url('assets/images/blog/lulu01.jpg')"></div></a>
                {{--<img src="{{asset('assets/images/blog/lulu01.jpg')}}" class="img-fluid blog_img" alt="">--}}
                <a href="#"><h3 class="card-title text-sm-center text-md-left">5 dôvodov prečo pracovať pre Lulubee</h3></a>
                <p>Do našej krásnej baby friendly predajne v bratislavskom River Parku si hľadáme kolegu. Práca v Lulubee je o osobnom kontakte, ale aj o samostatnej činnosti a kreativite. Stále váhate, či prísť práve k nám? Tu je pár poznatkov, ktoré by vás mohli presvedčiť.</p>
                <a href="#" class="btn btn-secondary">Čítaj viac</a>
            </div>

            <div class="col-md-4 clanok text-sm-center text-md-left">
                <a href="#"><div class="card-img-top" style="background:url('assets/images/blog/drevene-skladacie-hracky.jpg')"></div></a>
                {{--<img src="{{asset('assets/images/blog/drevene-skladacie-hracky.jpg')}}" class="img-fluid blog_img" alt="">--}}
                <a href="#"><h3 class="card-title text-sm-center text-md-left ">Francúzske hračky Janod prichádzajú do Lulubee</h3></a>
                <p>Je pre nás veľkým potešením, že vám môžeme predstaviť novinku v našej ponuke! V Lulubee vítame francúzskeho výrobcu Janod - značku, ktorá vďaka dlhoročnej tradícii vo výrobe drevených hračiek patrí k svetovej špičke! </p>
                <a href="#" class="btn btn-secondary">Čítaj viac</a>
            </div>

            <div class="col-md-4 clanok text-sm-center text-md-left">
                <a href="#"><div class="card-img-top" style="background-image: url('{{asset('assets/images/blog/leander-historia.jpg')}}')" ></div></a>
                {{--<img src="{{asset('assets/images/blog/leander-historia.jpg')}}"  class="img-fluid blog_img" alt="">--}}
                <a href="#"><h3 class="card-title text-sm-center text-md-left">Všetko to začalo kolískou pre dvojčatá...</h3></a>
                <p>História značky Leander sa začala písať v roku 1998. Stig Leander bol vyučeným kováčom, ktorý už dlhší čas pracoval na vývoji stoličky s vymeniteľným sedadlom
                    a chrbtom. Mal 28 rokov, keď jeho švagriná otehotnela a čakala dvojčatá.</p>
                <a href="#" class="btn btn-secondary">Čítaj viac</a>
            </div>
        </div>
    </div>

@endsection
