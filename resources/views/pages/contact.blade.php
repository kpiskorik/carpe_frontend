@extends('home')
@section('title', 'Home')

@section('content')
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('index.show')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Kontakt</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row" style="width: 80%; margin:0 auto;">
            <div class="col-12">
                <h1 class="contact">Kontakt Lulubee</h1>
            </div>
            <div class="col-md-8 col-sm-12 pl-5">
                <h2 class="mb-4">Máte otázku? Radi vám ju zodpovieme.</h2>
                <h1 class="contact">Kontakt</h1>
                <form>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputName">Meno</label>
                            <input type="text" class="form-control form-control-lg pr-5" id="inputName">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail" class="">Email</label>
                            <input type="email" class="form-control form-control-lg" id="inputEmail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSubject">Predmet</label>
                        <input type="text" class="form-control form-control-lg" id="inputSubject">
                    </div>
                    <div class="form-group">
                        <label for="inputMessage">Telo správy</label>
                        <input type="text" class="form-control form-control-lg" id="inputMessage">
                    </div>
                    {{--<div class="form-row">--}}
                        {{--<div class="form-group col-md-4 mt-4">--}}
                            {{--<img src="#" alt="Captcha">--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-4">--}}
                            {{--<label for="inputCaptcha">Opíšte text z obrázku</label>--}}
                            {{--<input type="text" class="form-control form-control-sm" id="inputCaptcha">--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-4 mt-4">--}}
                            {{--<button type="submit" class="btn btn-primary float-left">Odoslať</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <button type="submit" class="btn btn-primary float-left ml-0">Odoslať</button>

                </form>
            </div>
            <div class="col-sm-12 col-md-3 ml-5 mt-5 mt-md-0">
                <h2 class="contact">Adresa</h2>
                <p><strong>Cubicon</strong></p>
                <p>
                    <span>Staré grunty 24</span>
                    <br>
                    <span>841 04 Bratislava</span>
                </p>
                <p>
                    <span>Email -<strong> info@lulubee.sk</strong></span>
                    <br>
                    <span>Tel. -<strong> +421 911 012 064</strong></span>
                </p>
                <h2 class="contact">Otváracie hodiny</h2>
                <p>Každý deň od 10:00 do 20:00</p>
                <h2 class="contact">Ako nás nájdete?</h2>
                <p><strong>Sídlime v <a href="#">Cubicon Bratislava</a></strong>  , kde máme dizajnovú predajňu Lulubee, v ktorej si aj vy vyberiete niečo pre vaše dieťa. Ponúkame nábytok do detskej izby, kočíky, fusaky a iné šikovné drobnosti pre vás a vaše dieťa, ktoré zároveň stoja za pohľad.</p>
            </div>
        </div>
    </div>


@endsection
