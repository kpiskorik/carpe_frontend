@extends('home')
@section('title', 'Color')

@section('content')

    <div style="height: 700px;">

        <div class="wrapper">
            <div class="insider">
                <div class="row">
                    <div class="col-11">
                        <h3 class="atc-message">Vyberte si z nasledujúcich variant</h3>

                    </div>
                    <div class="col-1">
                    <a href="#"><img src="images/close.png" alt="close" class="atc-message close"></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 mt-4">
                        <img class="atc-image" src="images/Kocik1.png">
                    </div>
                    <div class="col-6">
                        <h3 class="atc-product_name mt-2">Detská autosedačka</h3>
                        <h3 class="atc-product_name">Cybex Aton 5</h3>
                        <h5 class="atc-product">(Kód produktu: 64985) </h5>
                        <p class="atc-text mt-5">Vyberte si variant</p>
                        <div class="farby">
                            <div class="pallete">
                                <a href="#">
                                    <div class="color" style="background:lightcoral;">
                                        <img src="images/fajnka.png" class="fajka" alt="yes">
                                    </div>
                                </a>
                                <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                                <a href="#"><div class="color" style="background:lightblue;"></div></a>
                                <a href="#"><div class="color" style="background:lightgoldenrodyellow;"></div></a>
                                <a href="#"><div class="color" style="background:lightslategrey;"></div></a>
                                <a href="#"><div class="color" style="background:lightgreen;"></div></a>
                            </div>
                        </div>
                        <p class="atc-text mt-1">Vami zvolená farebná variácia</p>
                        <div class="color float-left" style="background:lightcoral;"> </div> <p class="atc-color-name">Light coral</p>

                        <div class="circle"><span class="otaznik">?</span></div> <p class="atc-pink" >Na objednávku - U vás doma do 3 týždňov</p>

                        <a class="btn btn-primary btn-sm float-right mt-2 mr-5" href="#">Dokončiť objednávku</a>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

    </div>

@endsection