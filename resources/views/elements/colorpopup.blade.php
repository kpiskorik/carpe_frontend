@extends('home')
@section('title', 'Cart')

@section('content')

<div style="height: 700px;">

</div>
<div class="wrapper" style="height: 600px;">
    <div class="insider">
        <div class="row ">
            <div class="col-12 text-center">
                <img src="images/yescircle.png" alt="yes" class="mt-4">
                <p class="atc-text text-center mt-3">Produkt bol úspešne pridaný do košíka</p>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-6 mt-4">
                <img class="atc-image" src="images/Kocik1.png">
            </div>
            <div class="col-6">
                <h3 class="atc-product_name mt-2">Detská autosedačka</h3>
                <h3 class="atc-product_name">Cybex Aton 5</h3>
                <h5 class="atc-product">(Kód produktu: 64985) </h5>
                <p class="atc-text mt-2" style="margin-bottom: 0px;">1 x 146,94€</p>
                <p class="atc-text mt-0 mb-4">146,94€</p>

                <div class="color float-left" style="background:lightcoral;"> </div> <p class="atc-color-name">Light coral</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-6">
                <div class="circle ml-5"><span class="info">i</span></div> <p class="atc-grey" >Nakúpte ešte za 10,04€ a vybrané typy dopravy máte zadarmo.</p>
            </div>
            <div class="col-3">
                <img src="images/bag-b.png" alt="nakup" class="float-left mr-3"> <p class="atc-text float-left mt-2">1 položka</p>
            </div>
            <div class="col-3" style="margin-top: -20px;">
                <p class="atc-text">Celková suma</p>
                <p class="cena" style="margin-left: 18px;    margin-top: -15px;  font-weight: 800;">146,94€</p>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-6">
                <a class="btn btn-primary btn-sm float-left mt-2 ml-5 pading-normal" href="#">Späť k nákupu</a>
            </div>
            <div class="col-6">
                <a class="btn btn-primary btn-sm float-right mt-2 mr-5" href="#">Dokončiť objednávku</a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>


@endsection