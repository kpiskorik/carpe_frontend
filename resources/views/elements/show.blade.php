@extends('layouts.app')
@section('title', 'Home')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-md-center" style="margin:50px;">
            <div class="col-lg-2">
                <a href="#" class="btn btn-primary btn-sm">Detail</a>
                <a href="#" class="btn btn-primary">Kupit</a>
                <a href="#" class="btn btn-primary">Spat k nakupu</a>
                <a href="#" class="btn btn-primary btn-lg">Velky button</a>
            </div>

            <div class="col-lg-2">
                <div class="social_media_links" style="margin-top:50px;">
                    <ul class="menu">
                        <li><a href="" class="fab fa-facebook-square social_link"></a></li>
                        <li><a href="" class="fab fa-instagram social_link"></a></li>
                        <li><a href="" class="fab fa-google-plus-g social_link"></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4">
                <form>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="First name*">
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" id="" placeholder="Last name*">
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" id="" placeholder="Adress line*">
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" id="" placeholder="Postal/Zip code*">
                        </div>

                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" id="" placeholder="Phone number">
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="email" class="form-control" id="" placeholder="Email*">
                    </div>

                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Billing adress is the same as shipping address</label>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-6">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Technické parametre</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Otázky</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Hodnotenia</a>
                    </li>
                </ul>


                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h4>Prehľad</h4>
                        <p>Stolička Tripp Trapp Stokke dopraje vášmu dieťatku maximálne pohodlie a komfort.</p>
                        <h4>Vlastnosti</h4>
                        <ul>
                            <li>veľmi jednoduchá montáž stoličky</li>
                            <li>veľmi jednoduché nastavenie</li>
                            <li>šesťboká kľučka, ktorá je súčasťou balenia</li>
                            <li>povrchová úprava uľahčuje čistenie stoličky</li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"></div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab"></div>
                </div>
            </div>
            </div>
        </div>



    </div>

@endsection