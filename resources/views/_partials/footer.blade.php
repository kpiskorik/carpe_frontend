<!-- Newsletter section -->
<section class="newsletter">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3 col-md-2 newsletter__heading">
				<img src="{{asset('assets/images/logo-news.png')}}" class="heading--img veralign-middle" alt="img newsletter">
              	<h4 class="heading--news veralign-middle">Newsletter</h4>
			</div>
			<div class="col-lg-6 col-md-8 newsletter__form">
				<form>
					<div class="form-row">
						<div class="col-lg-7 col-md-6 form--input">
                    		<input type="email" class="form-control form-control-lg" placeholder="Napíšte svoj e-mail">
                    	</div>
                    	<div class="col-lg-5 col-md-6 form--button">
                    		<button type="submit" class="btn btn-block btn-lg btn-custom">Prihlásiť sa na odber</button>
                    	</div>
                    	<div class="col-md-12 form--check">
                    		<input type="checkbox" class="custom-control-input" id="customControlInline">
                  			<label class="custom-control-label" for="customControlInline">Potvrdenim vyjadrujem suhlas so <a href="#">spracovanim osobnych udajov</a></label>
                    	</div>
                  </div>
				</form>
			</div>
			<div class="col-lg-3 col-md-2 newsletter__sales">
				<img src="{{asset('assets/images/news_cart.png')}}" class="sales--img veralign-middle" alt="salse img">
				<div class="veralign-middle">
					<p class="sales--text">Pozvite priatelov a ziskajte</p>
                	<h4 class="sales--head">10€</h4>
                </div>
			</div>
		</div>
	</div>
</section>

<!-- Logo Slider section -->
<section class="logo-carousel">
    <div class="container-fluid">
        <!--<div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="9000">
            <div class="carousel-inner row mx-auto" role="listbox">
                              <div class="carousel-item col-md-2 active">
                                    <img class="img-fluid mx-auto d-block" src="{{asset('assets/images/logos/logos_01.png')}}" alt="slide 1">
                                </div>
                                <div class="carousel-item col-md-2">
                                    <img class="img-fluid mx-auto d-block" src="{{asset('assets/images/logos/logos_02.png')}}" alt="slide 2">
                                </div>
                                <div class="carousel-item col-md-2">
                                    <img class="img-fluid mx-auto d-block" src="{{asset('assets/images/logos/logos_03.png')}}" alt="slide 3">
                                </div>
                                <div class="carousel-item col-md-2">
                                    <img class="img-fluid mx-auto d-block" src="{{asset('assets/images/logos/logos_04.png')}}" alt="slide 4">
                                </div>
                                <div class="carousel-item col-md-2">
                                    <img class="img-fluid mx-auto d-block" src="{{asset('assets/images/logos/logos_05.png')}}" alt="slide 5">
                                </div>
                                <div class="carousel-item col-md-2">
                                    <img class="img-fluid mx-auto d-block" src="{{asset('assets/images/logos/logos_06.png')}}" alt="slide 6">
                                </div>
                                <div class="carousel-item col-md-2">
                                    <img class="img-fluid mx-auto d-block" src="{{asset('assets/images/logos/logos_03.png')}}" alt="slide 7">
                                </div>
                                <div class="carousel-item col-md-2">
                                    <img class="img-fluid mx-auto d-block" src="{{asset('assets/images/logos/logos_03.png')}}" alt="slide 8">
                                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                <i class="fa fa-chevron-left fa-lg text-muted"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
                <i class="fa fa-chevron-right fa-lg text-muted"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>-->

        <div class="carousel-wrap">
          <div class="owl-carousel" id="logo-carousel">
            <div class="item"><img src="{{asset('assets/images/logos/logos_01.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_02.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_03.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_04.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_05.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_06.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_01.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_02.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_03.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_04.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_05.png')}}"></div>
            <div class="item"><img src="{{asset('assets/images/logos/logos_06.png')}}"></div>
          </div>
        </div>


    </div>
</section>

<!-- Footer section -->
<footer class="footer">
	<div class="container-fluid">
		<div class="row footer__row">
			<div class="col-lg-2 order-lg-1 col-md-3 order-md-1 col-sm-6 order-sm-1 order-1 footer__section">
				<h3 class="footer--heading">O Lulubee</h3>

				<ul class="list-unstyled">
                    <li><a href="#">O nás</a></li>
                    <li><a href="#">Kontakt</a></li>
                    <li><a href="#">Značky</a></li>
                    <li><a href="#">Služby pre zákaznikov</a></li>
                    <li><a href="#">Servis kočíkov</a></li>
                    <li><a href="#">Hladáme do lulubee tímu</a></li>
                </ul>
			</div>
			<div class="col-lg-2 order-lg-2 col-md-3 order-md-2 col-sm-6 order-sm-4 order-2 footer__section footer__section__info">

				<h3 class="footer--heading">Informácie</h3>
				<ul class="list-unstyled">
                   <li><a href="#">Obchodné podmienky</a></li>
                   <li><a href="#">Ochrana osobných údajov</a></li>
                   <li><a href="#">Reklamácie</a></li>
                   <li><a href="#">Spôsob dopravy a platby</a></li>
               </ul>
			</div>
			<div class="col-lg-4 order-lg-3 col-md-12 order-md-5 col-sm-12 order-sm-5 order-5 footer__section footer__section__social">

				<img src="{{asset('assets/images/footer--logo.png')}}" class="img-fluid footer--logo" alt="lulube footer logo">

				<p>Pripojte sa k našim socialnym sieťam</p>

				  <ul class="list-inline social--icons">
				    <li class="list-inline-item"><a href="#"><i class="fab fa-instagram fa-2x"></i></a></li>
				    <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f fa-2x"></i></a></li>
				    <li class="list-inline-item"><a href="#"><i class="fab fa-google-plus-g fa-2x"></i></a></li>
				  </ul>
			</div>
			<div class="col-lg-2 order-lg-4 col-md-3 order-md-3 col-sm-6 order-sm-3 order-3 footer__section footer__section__hours">
				<h3 class="footer--heading">Otvaracie hodiny</h3>

				<ul class="list-unstyled hours--list">
                   <li>PO / 14:00 - 19:00</li>
                   <li>UT - PI / 11:00 - 19:00</a></li>
                   <li>SO / 10:00 - 17:00</li>
                   <li>NE / zatvorené</a></li>
               	</ul>
			</div>
			<div class="col-lg-2 order-lg-5 col-md-3 order-md-4 col-sm-6 order-sm-2 order-4 footer__section footer__section__adress">
				<div class="adress--row">
					<h3 class="footer--heading">Adresa predajne</h3>

					<ul class="list-unstyled">
	                  <li>Lulubee</a></li>
	                  <li>Dvořakovo nábriezie 8</li>
	                  <li>811 02 Bratislava</li>
	                  <li class="email">Email - info@lulubee.sk</li>
	                  <li>Tel. - +421 911 012 064</li>
	              	</ul>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Aside section -->
<div class="overlay__white"></div>
<div class="aside__topmenu">
    <div class="aside__topmenu__header">
        <h3 class="aside__topmenu__header__title">Navigation</h3>
        <a href="javascript:void(0)" class="aside__topmenu__header__close">
            <img src="{{asset('assets/images/close.png')}}" class="img-fluid" alt="close img">
        </a>
    </div>
    <div class="aside__topmenu__content">
        <ul class="list-group">
            <li class="list-group-item"><a href="#">predajne</a></li>
            <li class="list-group-item"><a href="#">klub lulubee</a></li>
            <li class="list-group-item"><a href="#">blog</a></li>
            <li class="list-group-item"><a href="#">košík</a></li>
            <li class="list-group-item"><a href="#">pomoc pri nákupe</a></li>
            <li class="list-group-item"><a href="#">10€ na prvý nákup</a></li>
        </ul>
    </div>