<!-- Header section -->
<header class="header">

	<div class="header__topbar">
		<div class="container-fluid">
			<div class="row header__topbar__row">
				<div class="col-xl-6 col-lg-7 col-md-9 col-sm-6 left">
					<ul class="list-inline header__topbar__links">
						<li class="list-inline-item hidden topbar__trigger"><a href="#">
							<button>
							    <img src="{{asset('assets/images/menu.svg')}}" alt="menu bar">
							 </button>
						</a></li>
						<li class="list-inline-item"><a href="#">predajne</a></li>
						<li class="list-inline-item"><a href="#">klub lulubee</a></li>
						<li class="list-inline-item"><a href="#">blog</a></li>
						<li class="list-inline-item"><a href="#">košík</a></li>
						<li class="list-inline-item"><a href="#">pomoc pri nákupe</a></li>
						<li class="list-inline-item bold"><a href="#">10€ na prvý nákup</a></li>
					</ul>
				</div>
				<div class="col-xl-6 col-lg-5 col-md-3 col-sm-6 right">
					<ul class="list-inline text-right header__topbar__contacts">
						<li class="list-inline-item phone"><img src="{{asset('assets/images/phone-btn.png')}}" alt="phone img">+421 911 012 064</li>
						<li class="list-inline-item time"><img src="{{asset('assets/images/clock-btn.png')}}" alt="clock img">PO-PIA: 10:00 - 18:00</li>
						<li class="list-inline-item time">SO: 10:00 - 13:00</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<nav class="header__navbar">
		<div class="container-fluid">
			<div class="row">
				<div class="col header__navbar__logo">
					<a href="#"><img src="{{asset('assets/images/logo.png')}}" class="img-fluid" alt="img logo"></a>
				</div>
				<div class="col header__navbar__menu">
					<ul class="nav">
					  <li class="nav-item button navbar__trigger">
					    <a class="nav-link" href="#">

					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#">kočíky</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#">autosedačky</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#">detská izba</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#">hračky</a>
					  </li>
					</ul>
				</div>
				<div class="col header__navbar__admin">
					  <ul class="nav justify-content-end">
							<li class="nav-item"><a class="nav-link" href="#">
								<img src="{{asset('assets/images/heart-icon.png')}}" alt="like icon">
							</a></li>
							<li class="nav-item"><a class="nav-link" href="#">
								<img src="{{asset('assets/images/user-icon.png')}}" alt="like icon">
							</a></li>
							<li class="nav-item"><a class="nav-link" href="#">
								<img src="{{asset('assets/images/shop-icon.png')}}" alt="like icon">
							</a></li>
							<li class="nav-item"><a class="nav-link" href="#">
								<img src="{{asset('assets/images/search-icon.png')}}" alt="search icon">
							</a></li>
					  </ul>
				</div>
			</div>
		</div>
	</nav>
</header>