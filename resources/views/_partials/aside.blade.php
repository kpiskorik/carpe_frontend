<div id="accordion" class="main__aside__menu__list">
	  <div class="card">
	    <div class="card-header" id="headingOne">
	      <h5 class="mb-0">
	        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
	          kočíky
	          <i class="card-icon card-icon-1 fa fa-chevron-down"></i>
	        </button>
	      </h5>
	    </div>
	    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
	      <div class="card-body">
	        <ul class="list__dropdown">
	          <li class="list__dropdown__item"><a href="#">Kočíky Bugaboo</a></li>
	          <li class="list__dropdown__item"><a href="#">Kočíky Stokke</a></li>
	          <li class="list__dropdown__item"><a href="#">Kočíky Joolz</a></li>
	          <li class="list__dropdown__item"><a href="#">Kočíky Cybex</a></li>
	          <li class="list__dropdown__item"><a href="#">Kočíky Babyzen</a></li>
	          <li class="list__dropdown__item"><a href="#">Prebaľovacie tašky</a></li>
	        </ul>
	      </div>
	    </div>
	  </div>
	  <div class="card">
	    <div class="card-header" id="headingTwo">
	      <h5 class="mb-0">
	        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
	          autosedačky
	          <i class="card-icon card-icon-1 fa fa-chevron-down"></i>
	        </button>
	      </h5>
	    </div>
	    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
	      <div class="card-body">
				<ul class="list__dropdown">
				  <li class="list__dropdown__item"><a href="#">Autosedačky 0-13 kg</a></li>
				  <li class="list__dropdown__item"><a href="#">Autosedačky 0-18 kg</a></li>
				  <li class="list__dropdown__item"><a href="#">Autosedačky 9-36 kg</a></li>
				  <li class="list__dropdown__item"><a href="#">Autosedačky 15-36 kg</a></li>
				  <li class="list__dropdown__item"><a href="#">Príslušenstvo autosedačky</a></li>
				</ul>
	      </div>
	    </div>
	  </div>
	  <div class="card">
	    <div class="card-header" id="headingThree">
	      <h5 class="mb-0">
	        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
	          detská izba
	          <i class="card-icon card-icon-1 fa fa-chevron-down"></i>
	        </button>
	      </h5>
	    </div>
	    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
	      <div class="card-body">
	        <ul class="list__dropdown">
	          <li class="list__dropdown__item">Nábytok Leander</li>
	          <li class="list__dropdown__item">Nábytok Mimm</li>
	          <li class="list__dropdown__item">Nábytok Stokke</li>
	          <li class="list__dropdown__item">Nábytok a doplnky Done by Deer</li>
	        </ul>
	      </div>
	    </div>
	  </div>
	  <div class="card">
	    <div class="card-header" id="headingFour">
	      <h5 class="mb-0">
	        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
	          hračky
	          <i class="card-icon card-icon-1 fa fa-chevron-down"></i>
	        </button>
	      </h5>
	    </div>
	    <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
	    	<div class="card-body">
		    	<ul id="accordion-2" class="list__dropdown">

		    		<li class="list__dropdown__item">Plyšové hračky pre najmenších</li>

					<li class="list__dropdown__item collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">Bábiky<i class="card-icon card-icon-2 fa fa-chevron-down"></i></li>
					<div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion-2">

					        <ul class="list__dropdown">
					          <li class="list__dropdown__item"><a href="#">Bábiky pre najmenších</a></li>
					          <li class="list__dropdown__item"><a href="#">Handrové bábiky</a></li>
					          <li class="list__dropdown__item"><a href="#">Kočíky pre bábiky</a></li>
					          <li class="list__dropdown__item"><a href="#">Doplnky pre bábiky</a></li>
					        </ul>

					</div>
					<li class="list__dropdown__item">Drevenné hračky</li>
					<li class="list__dropdown__item">Kuchynky pre deti</li>
		    	</ul>
		    </div>
	    </div>
	  </div>
	  <div class="card">
	    <div class="card-header" id="headingFive">
	      <h5 class="mb-0">
	        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
	          doplnky pre deti
	          <i class="card-icon card-icon-1 fa fa-chevron-down"></i>
	        </button>
	      </h5>
	    </div>
	    <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
	      <div class="card-body">
	        <ul class="list__dropdown">
	          <li class="list__dropdown__item">Hygiena</li>
	          <li class="list__dropdown__item">Kŕmenie</li>
	          <li class="list__dropdown__item">Nosiče</li>
	        </ul>
	      </div>
	    </div>
	  </div>
	  <div class="row">
	  	<div class="col-md-12">
	  		<ul class="list-inline text-left aside-phone">
				<li class="list-inline-item phone"><i class="fas fa-phone"></i> + 421 911 012 064</li>
			</ul>
	  	</div>
	</div>
</div>