<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lulubee | @yield( 'title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('assets/front/css/front.css')}}">


</head>
<body>

    @include('partials.header')

    <main>

    @yield ('content')

    </main>



    <script src="{{asset('assets/js/app.js')}}"></script>

    @stack('scripts')

</body>

@include('partials.footer')

</html>