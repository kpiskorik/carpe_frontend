@extends('home')
@section('title', 'Home')

@section('content')
    <div class="container-fluid banners">
        <div class="row">
            <div class="col-md-6 container_images">
                <img src="{{asset('assets/images/home-new.jpg')}}" class="img-fluid" >
                <div class="img_label">Detská izba 40 % zľava</div>
            </div>
            <div class="col-md-4 container_images">
                <img src="{{asset('assets/images/kociky.jpg')}}" class="img-fluid" style="max-height:500px;">
                <div class="img_label">Detská izba 40 % zľava</div>
            </div>
            <div class="col-md-1 d-none d-xl-block">
                <div class="img_logos">
                    <img src="{{asset('assets/images/logos/logos_01.png')}}" class="img-fluid img_logo" alt="">
                    <img src="{{asset('assets/images/logos/logos_02.png')}}" class="img-fluid img_logo" alt="">
                    <img src="{{asset('assets/images/logos/logos_03.png')}}" class="img-fluid img_logo" alt="">
                    <img src="{{asset('assets/images/logos/logos_05.png')}}" class="img-fluid img_logo" alt="">
                    <img src="{{asset('assets/images/logos/logos_06.png')}}" class="img-fluid img_logo" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid main_content_category">
        <div class="row category_title">
            <div class="col-12">
                <h2>Obľubené kategórie</h2>
            </div>
        </div>
        <div class="row category_products">
            <div class="col-md-12">

                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#kociky" role="tab" aria-controls="home" aria-selected="false">Kočíky</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#sedacky" role="tab" aria-controls="profile" aria-selected="false">Sedačky</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#hracky" role="tab" aria-controls="contact" aria-selected="false">Hračky</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#izba" role="tab" aria-controls="contact" aria-selected="false">Izba</a>
                    </li>
                </ul>
                <div class="tab-content container-fluid" id="myTabContent">
                    <div class="tab-pane fade show active row ml-auto mr-auto" id="kociky" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="container-fluid pl-0">
                            <div class="product col-12 col-sm-6 col-lg-3 ">
                                <div class="akcia label">Akcia</div>
                                <div class="zlava label">20%</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                                <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3 ">
                                <div class="akcia label">Akcia</div>
                                <div class="zlava label">20%</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3">
                                <div class="top label">Top produkt</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3">
                                <div class="novinka label">Novinka</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                        </div>

                    </div>
                    <div class="tab-pane fade" id="sedacky" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="container-fluid pl-0">
                            <div class="product col-12 col-sm-6 col-lg-3 ">
                                <div class="akcia label">Akcia</div>
                                <div class="zlava label">20%</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                                <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3 ">
                                <div class="akcia label">Akcia</div>
                                <div class="zlava label">20%</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3">
                                <div class="top label">Top produkt</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3">
                                <div class="novinka label">Novinka</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                        </div>

                    </div>
                    <div class="tab-pane fade" id="hracky" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="container-fluid pl-0">
                            <div class="product col-12 col-sm-6 col-lg-3 ">
                                <div class="akcia label">Akcia</div>
                                <div class="zlava label">20%</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                                <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3 ">
                                <div class="akcia label">Akcia</div>
                                <div class="zlava label">20%</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3">
                                <div class="top label">Top produkt</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3">
                                <div class="novinka label">Novinka</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                        </div>

                    </div>
                    <div class="tab-pane fade" id="izba" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="container-fluid pl-0">
                            <div class="product col-12 col-sm-6 col-lg-3 ">
                                <div class="akcia label">Akcia</div>
                                <div class="zlava label">20%</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                                <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3 ">
                                <div class="akcia label">Akcia</div>
                                <div class="zlava label">20%</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3">
                                <div class="top label">Top produkt</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>

                            <div class="product col-12 col-sm-6 col-lg-3">
                                <div class="novinka label">Novinka</div>
                                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                                <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                                <a class="detail" href="#"><div class="btn btn-primary btn-sm float-right">Detail</div></a>
                                <a class="kupit" href="#"><div class="btn btn-primary btn-sm float-right">Kupit</div></a>
                                <span class="cena"><p>1079,60€</p></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid banners ">
        <div class="row">
            <div class="col-md-6 container_images text-center">
                <img src="{{asset('assets/images/baby-img.jpg')}}" class="img-fluid mb-lg-5">
                <div class="img_label">Detská postielka</div>
            </div>
            <div class="col-md-3 container_images text-center">
                <img src="{{asset('assets/images/baby1-img.jpg')}}" class="img-fluid mb-lg-5">
                <div class="under_img_label">
                    <p>Letny wishlist typ od Lulubee</p>
                </div>
            </div>
            <div class="col-md-3 container_images text-center">
                <img src="{{asset('assets/images/baby2-img.jpg')}}" class="img-fluid mb-lg-5">
                <div class="under_img_label brown">
                    <p>Dizajnove kusky kocikov</p>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="container-fluid services">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center popis_servis">
                <img src="{{asset('assets/images/services/services-car.jpg')}}"  class="img-fluid services-img" alt="">
                <h3>Doprava zdarma od 100€</h3>
                <p>Pri nákupe akéhokoľvek produktu z našej ponuky, získavate, ako bonus dopravu k vám domov úplne zdarma.</p>
            </div>

            <div class="col-md-4 text-center popis_servis">
                <img src="{{asset('assets/images/services/services-map.jpg')}}" class="img-fluid services-img" alt="">
                <h3>Osobný odber</h3>
                <p>Našu kamennú predajňu nájdete na adrese <span class="adress">Dvořákovo nábrežie 8, 811 02 Bratislava</span></p>
            </div>

            <div class="col-md-4 text-center popis_servis">
                <img src="{{asset('assets/images/services/services-kosik.jpg')}}" class="img-fluid services-img" alt="">
                <h3>Servis kočíkov</h3>
                <p>Okrem predaja prémiového tovaru ponúkame tiež služby týkajúce sa čistenia kočíkov, skladania zakúpeného tovaru, jeho vypožičania a iné.</p>
            </div>
        </div>
    </div>

    <div class="blog container-fluid">
        <div class="row blog_heading">
            <div class="col-md-12 text-center">
                <h2>Blog</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-sm-center text-md-left clanok">
                {{--<div class="blog_picture" style="background:url('assets/images/blog/lulu01.jpg')"></div>--}}
                <img src="{{asset('assets/images/blog/lulu01.jpg')}}" class="img-fluid blog_img" alt="">
                <h2 class="card-title text-sm-center text-md-left">5 dôvodov prečo pracovať pre Lulubee</h2>
                <p class="card-text">Do našej krásnej baby friendly predajne v bratislavskom River Parku si hľadáme kolegu. Práca v Lulubee je o osobnom kontakte, ale aj o samostatnej činnosti a kreativite. Stále váhate, či prísť práve k nám? Tu je pár poznatkov, ktoré by vás mohli presvedčiť.</p>
                <a href="#" class="btn btn-secondary mb-5">Citaj viac</a>
            </div>

            <div class="col-md-4 clanok text-sm-center text-md-left">
                {{--<div class="blog_picture" style="background:url('assets/images/blog/drevene-skladacie-hracky.jpg')"></div>--}}
                <img src="{{asset('assets/images/blog/drevene-skladacie-hracky.jpg')}}" class="img-fluid blog_img" alt="">
                <h2 class="card-title text-sm-center text-md-left ">Francúzske hračky Janod prichádzajú do Lulubee</h2>
                <p class="card-text ">Je pre nás veľkým potešením, že vám môžeme predstaviť novinku v našej ponuke! V Lulubee vítame francúzskeho výrobcu Janod - značku, ktorá vďaka dlhoročnej tradícii vo výrobe drevených hračiek patrí k svetovej špičke! </p>
                <a href="#" class="btn btn-secondary mb-5">Citaj viac</a>
            </div>

            <div class="col-md-4 clanok text-sm-center text-md-left">
                {{--<div class="blog_picture" style="background:url('assets/images/blog/drevene-skladacie-hracky.jpg')"></div>--}}

                <img src="{{asset('assets/images/blog/leander-historia.jpg')}}"  class="img-fluid blog_img" alt="">
                <h2 class="card-title text-sm-center text-md-left">Všetko to začalo kolískou pre dvojčatá...</h2>
                <p class="card-text">História značky Leander sa začala písať v roku 1998. Stig Leander bol vyučeným kováčom, ktorý už dlhší čas pracoval na vývoji stoličky s vymeniteľným sedadlom
                    a chrbtom. Mal 28 rokov, keď jeho švagriná otehotnela a čakala dvojčatá.</p>
                <a href="#" class="btn btn-secondary mb-5">Citaj viac</a>
            </div>
        </div>
    </div>
@endsection

{{--<div class="farby">--}}
{{--<div class="pallete">--}}
{{--<a href="#"><div class="color" style="background:blue;"></div></a>--}}
{{--<a href="#"><div class="color" style="background:lightsalmon;"></div></a>--}}
{{--<a href="#"><div class="color" style="background:lightblue;"></div></a>--}}
{{--<a href="#"><div class="color" style="background:lightcoral;"></div></a>--}}
{{--<a href="#"><div class="color" style="background:lightslategrey;"></div></a>--}}
{{--</div>--}}
{{--</div>--}}