@extends('layouts.app')
@section('title', 'Product')

@section('content')

    <div class="product_detail container-fluid">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
            </div>
            <div class="col-12 col-lg-6">
                <div class="pictures container-fluid">
                    <div class="row">
                        <div class="gallery col-1 d-none d-sm-block ">
                            <a href="#"><img src="{{ asset('images/Trip%20Trapp%202.png') }}" alt="miniview" width="70"></a>
                            <a href="#"><img src="{{ asset('images/Trip%20Trapp%203.png') }}" alt="miniview" width="100"></a>
                            <a href="#"><img src="{{ asset('images/Trip%20Trapp.png') }}" alt="miniview" width="100"></a>
                            <a href="#"><img src="{{ asset('images/Trip%20Trapp.png') }}" alt="miniview" width="100"></a>
                        </div>
                        <div class="displayedPicture col-5">
                            <img src="images/Trip%20Trapp%204.png" alt="" width="250" id="imageHolder">
                        </div>
                    </div>
                </div>
            </div>

            <div class="productDescription col-12 col-lg-6">
                <div class="title">
                    <h1>Detská stolička </h1>
                    <h1> Tripp Trapp Chair</h1>
                    <img class="d-none d-md-block bigimg" src="images/stokke-logo_0.png" alt="" width="130">
                    <img class="d-sm-block d-md-none smallImage mt-3 mb-3" src="images/stokke-logo_0.png" alt="" width="130">
                </div>
                <div class="stars float-none mb-2">
                    <ul class="menu pl-0">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                    </ul>
                </div>
                <p class="oldPrice ml-0">1129€</p>
                <span class="cena pl-0" ><p class="ml-0" > 1079,60€</p></span>
                <div class="farby">
                    <p style="width: 100px;float:left;margin-top: 8px;width: 15%"> Farba</p>
                    <div class="float-left" style="width: 85%">
                        <div class="circle mt-2"><span class="otaznik mt-1" style="margin-left: 1px;">?</span></div>
                        <p class="atc-pink" style="margin-top: 10px;">Na objednávku - U vás doma do 3 týždňov</p>
                    </div>

                    <div class="farby">
                        <div class="pallete">
                            <a href="#"><div class="color" style="background:lightseagreen;"></div></a>
                            <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                            <a href="#"><div class="color" style="background:lightblue;"></div></a>
                            <a href="#"><div class="color" style="background:lightcoral;"></div></a>
                            <a href="#"><div class="color" style="background:lightslategrey;"></div></a>
                        </div>
                    </div>
                </div>
                <a href="#" class="btn btn-primary float-left">Kúpiť</a>
                <div class="count">
                    <ul class="menu">
                        <li><a href="#"><span class="fa fa-minus-circle"></span></a></li>
                        <li>1</li>
                        <li><a href="#"><span class="fa fa-plus-circle"></span></a></li>
                    </ul>
                </div>

                <div style="float: left;width: 100%">
                    <ul class="menu pl-0 mt-2 mb-2">
                        <li style="width: 30%"><img src="images/warehouse.png" alt="warehouse" width="30px" class="mr-2">Skladom</li>
                        <li style="width:30%"><img src="images/car.png" alt="car" width="30px" class="mr-2">Doprava zdarma</li>
                        <li style="width:30%"><img src="images/heart.png" alt="heart" width="30px" class="mr-2">Pridať k obľúbeným</li>
                    </ul>
                </div>
                <div class="technical_parameters">
                    <h3>Technické parametre</h3>
                    <table>
                        <tr>
                            <td>Materiál</td>
                            <td>buk</td>
                        </tr>
                        <tr>
                            <td>Rozmer</td>
                            <td>45,5 x 49 x 78</td>
                        </tr>
                        <tr>
                            <td>Maximálne zaťaženie (kg)</td>
                            <td>80</td>
                        </tr>
                        <tr>
                            <td>Hmotnosť (kg)</td>
                            <td>7,16</td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="container-fluid mt-5">
        <div class="row ">
            <div class="col-md-6 col-12 pl-md-5 pl-3" >
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active pl-0" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false"><h2>Technické parametre</h2></a>
                    </li>
                    <li style="color:#7f7f7f" class="mt-2 pr-3"><h2>|</h2></li>


                    <li class="nav-item">
                        <a class="nav-link pl-0" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><h2>Otázky</h2></a>
                    </li>
                    <li style="color:#7f7f7f" class="mt-2 pr-3"><h2>|</h2></li>

                    <li class="nav-item mt-0">
                        <a class="nav-link pl-0" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><h2>Hodnotenia</h2></a>
                    </li>
                </ul>

                <div class="tab-content pr-5" id="myTabContent">
                    <div class="tab-pane fade show active description" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h3>Prehľad</h3>
                        <p>Stolička Tripp Trapp Stokke dopraje vášmu dieťatku maximálne pohodlie a komfort. Je vyrobená z pružného bukového dreva. Inteligentne nastaviteľná konštrukcia umožňuje voľnosť pohybu. Hlavnou prednosťou tejto stoličky je správne nastaviteľné ergonomické držanie polohy tela a podporuje vývoj chrbtice vášho dieťaťa. Umožňuje nastavovanie výšky sedu a opierky nožičiek. Stolička sa dá nastaviť podľa veľkosti dieťaťa. Poskytuje optimálnu polohu pre jedlo. Stolička Tripp Trapp Stokke je vhodná pre deti do 6 mesiacov. Vyrába sa v mnohých farebných prevedeniach, a tak si môžete vybrať to, ktoré najviac pasuje do vášho interiéru.</p>
                        <h3>Vlastnosti</h3>
                        <ul>
                            <li>veľmi jednoduchá montáž stoličky</li>
                            <li>veľmi jednoduché nastavenie</li>
                            <li>šesťboká kľučka, ktorá je súčasťou balenia</li>
                            <li>povrchová úprava uľahčuje čistenie stoličky</li>
                        </ul>
                        <h3>Čo je v krabici</h3>
                        <p>Tripp Trapp je stolička, ktorá rastie s dieťatkom. Vysoká detská stolička spôsobila revolúciu v kategórii detských stoličiek v roku 1972, kedy bola prvýkrát uvedená na trh. Je navrhnutá tak, aby sa zmestila priamo pod jedálenský stôl, takže vaše dieťa je v srdci rodiny, čo mu umožňuje sa učiť a rozvíjať spolu s vami.</p>

                        <p>Nórsky výrobca Stokke je svetoznáma spoločnosť, ktorá produkuje kvalitný a originálny detský nábytok, doplnky pre deti a dizajnové detské kočíky. Spoločnosť Stokke nadväzuje na dlhoročnú tradíciu výroby produktov, ktoré podporujú zdravý vývin detí a posilňujú puto medzi dieťaťom a rodičom. Potreby dieťaťa sú u výrobcu Stokke najvyššou prioritou.</p>

                        <h3>Údržba</h3>
                        <ul>
                            <li>otrite čistou vlhkou handričkou a odstráňte prebytočnú vodu suchou handričkou. Dlhodobá vlhkosť môže spôsobiť praskanie materiálu</li>
                            <li>nepoužívajte žiadne vreckovky ani mikrovlákno</li>
                            <li>farby sa môžu zmeniť, ak je drevo vystavené slnku</li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <h1>Otazky</h1>
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <h1>Hodnotenia</h1>
                    </div>
                </div>

            </div>

            <div class="col-md-6 col-12 pl-sm-5 pl-md-0 mt-md-2 mt-sm-5">
                <h2>Odporúčame k produktu</h2>
                <div class="row">
                    <div class="product col-12 col-md-6">
                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-md-6">
                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-md-6">
                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>

                    <div class="product col-12 col-md-6">
                        <div class="akcia label">Akcia</div>
                        <div class="zlava label">20%</div>
                        <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                        <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                        <h4>Kočík Bugaboo Donkey2 Complete</h4>
                        <div class="buttons">
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                            <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                        </div>
                        <div class="stars">
                            <ul class="menu">
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                                <li><a href=""><i class="far fa-star"></i></a></li>
                            </ul>
                        </div>
                        <span class="cena"><p>1079,60€</p></span>
                    </div>
                </div>


                <div class="clearfix"></div>

        </div>
    </div>

@endsection