<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lulubee | @yield( 'title')</title>

    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">

</head>
<body>

	@include('partials.header')

	<main class="main main--products">
		<div class="main__aside__menu">
			{{--@include('_partials.aside')--}}
		</div>

		<div class="main__content">
			<div class="container-fluid">
				@yield ( 'content')
			</div>
		</div>
		<div class="overlay__white"></div>

	</main>



    @include('partials.footer')

    <script src="{{asset('assets/js/app.js')}}"></script>

</body>
</html>