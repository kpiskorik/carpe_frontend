@extends('home')
@section('title', 'list')

@section('content')
    <div class="menu_stroller container-fluid">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Category</li>
                    </ol>
                </nav>
            </div>
            <div class="col-lg-6 category_top_choice">
                <h1>Kociky</h1>
                <div class="row mb-4 mt-4">
                    <div class="col-12 col-sm-6 col-md-4 filter_top">
                        <ul>
                            <li><a href="#"> <i class="fas fa-arrow-right"></i> Kociky od narodenia</a></li>
                            <li><a href="#"><i class="fas fa-arrow-right"></i> Kombinovane kociky</a></li>
                            <li><a href="#"> <i class="fas fa-arrow-right"></i> Kociky od narodenia</a></li>
                            <li><a href="#"><i class="fas fa-arrow-right"></i> Kombinovane kociky</a></li>
                        </ul>
                    </div>
                    <div class=" col-12 col-sm-6  col-md-4 filter_top">
                        <ul>
                            <li><a href="#"> <i class="fas fa-arrow-right"></i> Kociky od narodenia</a></li>
                            <li><a href="#"><i class="fas fa-arrow-right"></i> Kombinovane kociky</a></li>
                            <li><a href="#"> <i class="fas fa-arrow-right"></i> Kociky od narodenia</a></li>
                            <li><a href="#"><i class="fas fa-arrow-right"></i> Kombinovane kociky</a></li>
                        </ul>
                    </div>
                    <div class=" col-12 col-sm-6 col-md-4 filter_top">
                        <ul>
                            <li><a href="#"> <i class="fas fa-arrow-right"></i> Kociky od narodenia</a></li>
                            <li><a href="#"><i class="fas fa-arrow-right"></i> Kombinovane kociky</a></li>
                            <li><a href="#"> <i class="fas fa-arrow-right"></i> Kociky od narodenia</a></li>
                            <li><a href="#"><i class="fas fa-arrow-right"></i> Kombinovane kociky</a></li>
                        </ul>
                    </div>
                </div>

                <p>Škandinávska značka Bugaboo píše históriu dizajnových kočíkov už od roku 1994. Či už ste nový rodič, ktorý sa chystá vyraziť do sveta s rodinou, alebo moderný cestovateľ,
                    produkty Bugaboo vám uľahčia každodenný život a premenia vaše cesty na príjemný zážitok. V našej ponuke, okrem športových kočíkov, nájdete aj kočíky s hlbokou
                    vaničkou či príslušenstvo. Ak čakáte bábätko, určite si u nás pozrite aj bezpečné autosedačky,
                    detské postiteľky, či dizajnové hračky.</p>
            </div>
            <div class="col-lg-6">
                <img class="img-fluid mb-5" src="images/Headline photo.png" alt="">
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    {{--filter--}}

    {{--<div class="container-fluid filters">--}}

        {{--<button class="btn btn-primary float-left"><i class="fas fa-filter mr-2"></i>Filter</button>--}}

        {{--<ul class="menu float-sm-right float-left">--}}
            {{--<li class="mt-2 d-none d-md-block"><a href="#"> <i class="fas fa-th fa-2x"></i> </a></li>--}}
            {{--<li class="mt-2 d-none d-md-block"><a href="#"><i class="fas fa-list-ul fa-2x"></i></a></li>--}}
            {{--<li class="mt-2 d-none d-md-block"><a href="#"><i class="fas fa-bars fa-2x"></i></a></li>--}}
            {{--<li class="mr-0">--}}
                {{--<form method="get" class="woocommerce-ordering ">--}}
                    {{--<select name="sort" id="filter__sort" class="orderby">--}}
                        {{--<option value="top">Najpredávanejšie</option>--}}
                        {{--<option value="pricea">Najlacnejšie</option>--}}
                        {{--<option value="priced">Najdrahšie</option>--}}
                    {{--</select>--}}
                {{--</form>--}}
            {{--</li>--}}
        {{--</ul>--}}

    {{--</div>--}}

    <div class="clearfix"></div>

    <div class="container-fluid">
        <div class="content_list row">

            <div class="product col-12 col-sm-6 col-lg-3 ">

                <div class="akcia label">Akcia</div>
                <div class="zlava label">20%</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="farby">
                    <div class="pallete">
                        <a href="#"><div class="color small-color" style="background:lightseagreen;"></div></a>
                        <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                        <a href="#"><div class="color" style="background:lightblue;"></div></a>
                        <a href="#"><div class="color" style="background:lightcoral;"></div></a>
                        <a href="#"><div class="color small-color" style="background:lightslategrey;"></div></a>
                    </div>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3 ">
                <div class="akcia label">Akcia</div>
                <div class="zlava label">20%</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="farby">
                    <div class="pallete">
                        <a href="#"><div class="color small-color" style="background:lightseagreen;"></div></a>
                        <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                        <a href="#"><div class="color" style="background:lightblue;"></div></a>
                        <a href="#"><div class="color" style="background:lightcoral;"></div></a>
                        <a href="#"><div class="color small-color" style="background:lightslategrey;"></div></a>
                    </div>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3">
                <div class="top label">Top produkt</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="farby">
                    <div class="pallete">
                        <a href="#"><div class="color small-color" style="background:lightseagreen;"></div></a>
                        <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                        <a href="#"><div class="color" style="background:lightblue;"></div></a>
                        <a href="#"><div class="color" style="background:lightcoral;"></div></a>
                        <a href="#"><div class="color small-color" style="background:lightslategrey;"></div></a>
                    </div>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3">

                <div class="novinka label">Novinka</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="farby">
                    <div class="pallete">
                        <a href="#"><div class="color small-color" style="background:lightseagreen;"></div></a>
                        <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                        <a href="#"><div class="color" style="background:lightblue;"></div></a>
                        <a href="#"><div class="color" style="background:lightcoral;"></div></a>
                        <a href="#"><div class="color small-color" style="background:lightslategrey;"></div></a>
                    </div>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3 ">

                <div class="akcia label">Akcia</div>
                <div class="zlava label">20%</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>
                <img src="{{asset('assets/images/product-1.jpg')}}" alt="kocik" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="farby">
                    <div class="pallete">
                        <a href="#"><div class="color small-color" style="background:lightseagreen;"></div></a>
                        <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                        <a href="#"><div class="color" style="background:lightblue;"></div></a>
                        <a href="#"><div class="color" style="background:lightcoral;"></div></a>
                        <a href="#"><div class="color small-color" style="background:lightslategrey;"></div></a>
                    </div>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3 ">
                <div class="akcia label">Akcia</div>
                <div class="zlava label">20%</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-2.jpg')}}" alt="kocik" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="farby">
                    <div class="pallete">
                        <a href="#"><div class="color small-color" style="background:lightseagreen;"></div></a>
                        <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                        <a href="#"><div class="color" style="background:lightblue;"></div></a>
                        <a href="#"><div class="color" style="background:lightcoral;"></div></a>
                        <a href="#"><div class="color small-color" style="background:lightslategrey;"></div></a>
                    </div>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3">
                <div class="top label">Top produkt</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-3.jpg')}}" alt="komoda" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="farby">
                    <div class="pallete">
                        <a href="#"><div class="color small-color" style="background:lightseagreen;"></div></a>
                        <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                        <a href="#"><div class="color" style="background:lightblue;"></div></a>
                        <a href="#"><div class="color" style="background:lightcoral;"></div></a>
                        <a href="#"><div class="color small-color" style="background:lightslategrey;"></div></a>
                    </div>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="product col-12 col-sm-6 col-lg-3">

                <div class="novinka label">Novinka</div>
                <a href="#" class="favorite"> <i class="far fa-heart "> </i> </a>

                <img src="{{asset('assets/images/product-4.jpg')}}" alt="komoda" class="product_img">
                <h4>Kočík Bugaboo Donkey2 Complete</h4>
                <div class="buttons">
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/search.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Detail</a>
                    <a class="btn btn-primary btn-sm float-right " href="#"><img src="images/bag.png" alt="detail" width="20px" class="float-left ml-3 mr-3">Kúpiť</a>
                </div>
                <div class="farby">
                    <div class="pallete">
                        <a href="#"><div class="color small-color" style="background:lightseagreen;"></div></a>
                        <a href="#"><div class="color" style="background:lightsalmon;"></div></a>
                        <a href="#"><div class="color" style="background:lightblue;"></div></a>
                        <a href="#"><div class="color" style="background:lightcoral;"></div></a>
                        <a href="#"><div class="color small-color" style="background:lightslategrey;"></div></a>
                    </div>
                </div>
                <div class="stars">
                    <ul class="menu">
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                        <li><a href=""><i class="far fa-star"></i></a></li>
                    </ul>
                </div>
                <span class="cena"><p>1079,60€</p></span>
            </div>

            <div class="col-12 load-more mt-5">

                <a href="#" class="btn btn-primary load-more-btn" >Načítaj ďalšie</a>

                <ul class="float-md-right d-md-block d-table menu page-numbers pr-lg-5 pr-2 pl-0">
                    <li class="ml-3"><a href="#"><i class="fas fa-angle-left"></i></a></li>
                    <li class="ml-3"><a href="#">1</a></li>
                    <li class="ml-3"><a href="#">2</a></li>
                    <li class="ml-3"><a href=""><i class="fas fa-angle-right"></i></a></li>
                </ul>

            </div>
        </div>
    </div>


@endsection
